'use strict';

    const  express = require('express'),
      http = require('http'),
      path = require('path'),
      favicon = require('serve-favicon');
//create express app
const app = express();

    //setup the web server
    app.server = http.createServer(app);

    //settings
    /**
     * because you don’t want to make it easy for an attacker to figure what you are running The X-Powered-By header can be extremely useful to an attacker for building a site’s risk profile
    */
    app.disable('x-powered-by');
    app.enable('trust proxy');
    app.set('port', 3000);
    app.set('views', path.join(__dirname, 'views'));
    app.set('view cache', true);
    app.set('view engine', 'ejs');
    
    app.use(require('morgan')('dev'));
    app.use(require('compression')());
    app.use(require('serve-static')(path.join(__dirname, 'public')));

    app.get('/', function(req,res) {
        res.render('index');
    });


    app.get('/vr', function(req,res) {
        res.render('index');
        //res.sendFile(require('path').join(__dirname, 'index.html'));
    });


    //


    app.server.listen(app.get('port'), function(){
      console.log('Server is running on port ' + 3000);
    });
